require("dotenv").config();
const { Sequelize } = require("sequelize");
const Todo = require("../../models/Todo");
const User = require("../../models/User");

class MySQLDB {
  static sequelize = new Sequelize("todo_db", "todo", "todopassword", {
    host: "atbdb-test.uksouth.cloudapp.azure.com",
    port: "3306",
    dialect: "mysql",
  });
  static connect() {
    this.sequelize
      .authenticate()
      .then(() => {})
      .catch((error) => {});
  }
}

let db = {
  Todo: Todo(MySQLDB.sequelize, Sequelize.DataTypes),
  User: User(MySQLDB.sequelize, Sequelize.DataTypes),
};

db.User.hasMany(db.Todo, { as: "todos" });
db.Todo.belongsTo(db.User, {
  foreignKey: "userId",
});

module.exports = { db, MySQLDB };
