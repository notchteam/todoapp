require("dotenv").config();
const redis = require("redis");
class RedisDB {
  static client = redis.createClient({
    host: process.env.redisConnectionUrl,
    port: process.env.redisConnectionPort,
    password: process.env.redisPassword,
  });

  static connect() {
    this.client.on("connect", () => {});
    this.client.on("error", (error) => {});
  }
}
module.exports = RedisDB;
