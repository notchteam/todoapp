require("dotenv").config();
const jwt = require("jsonwebtoken");
const RedisDB = require("../database/redis/redis");
let userId;
const verifyToken = (request, response, next) => {
  // Take the token from the query parameter
  const token = request.query.secret_token;
  // Verify the token
  jwt.verify(token, process.env.secret_token, (error, decoded) => {
    if (error) {
      response.status(401).send({
        success: null,
        message: error.message,
      });
    } else {
      // Append the parameters to the request object
      request.userId = decoded._id;
      request.tokenExp = decoded.exp;
      request.token = token;

      // assign userId to decoded userId to use in redisDB
      userId = decoded._id;
      RedisDB.client.get(token, (err, reply) => {
        if (err) {
          return response.status(403).json({
            code: "FAILED",
            success: null,
            error: err.message,
          });
        }
        if (reply === userId) {
          next();
        }
      });
    }
  });
};

module.exports = verifyToken;
