require("dotenv").config();
const UserService = require("../../services/User");
const jwt = require("jsonwebtoken");
const RedisDB = require("../../database/redis/redis");
const bcrypt = require("bcrypt");
const { db } = require("../../database/sequelize/sequelize");
const User = db.User;

class UserController {
  static async login(req, res) {
    const email = req.body.email;
    const password = req.body.password;

    const [user] = await User.findAll({ where: { email: email } });
    if (user) {
      await bcrypt.compare(password, user.password, function (err, result) {
        if (result) {
          const token = jwt.sign(
            { _id: user.id, email: user.email },
            process.env.secret_token,
            { expiresIn: "7d" }
          );
          RedisDB.client.set(`${token}`, `${user.id}`);

          return res.status(200).json({
            message: "Successfully logged in",
            token: token,
          });
        } else {
          return res.status(401).json({
            code: "FAILED",
            success: null,
            message: "Check password",
          });
        }
      });
    } else {
      return res.status(401).json({
        code: "FAILED",
        success: null,
        message: "User cannot be found check email address",
      });
    }
  }

  static async logout(req, res) {
    RedisDB.client.del(req.token, (err, reply) => {
      res.status(200).json({
        message: "Successfully Logged Out",
        error: null,
      });
    });
  }

  static async createUser(req, res) {
    let password = req.body.password;
    req.body.password = await bcrypt.hash(password, 10);
    try {
      let user = await UserService.createUser(req.body);
      if (user) {
        res.status(200).json({
          code: "SUCCESS",
          success: user,
          error: null,
        });
      } else {
        throw new Error("Cannot create user");
      }
    } catch (error) {
      res.status(400).json({
        code: "FAILED",
        success: null,
        error: error.message,
      });
    }
  }

  static async getUserByIdWithTodos(req, res) {
    try {
      let user = await UserService.getUserByIdWithTodos(req.userId);
      if (user) {
        user.password = null;
        res.status(200).json({
          code: "SUCCESS",
          success: user,
          error: null,
        });
      } else {
        throw new Error("Cannot find user");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async getUserByIdWithoutTodos(req, res) {
    let user = await UserService.getUserByIdWithoutTodos(req.userId);
    try {
      if (user) {
        res.status(200).json({
          code: "SUCCESS",
          success: user,
          error: null,
        });
      } else {
        throw new Error("Cannot find user");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async deleteUserById(req, res) {
    try {
      let deletedUser = await UserService.deleteUserById(req.userId);
      if (deletedUser) {
        res.status(200).json({
          code: "SUCCESS",
          success: deletedUser,
          error: null,
        });
      } else {
        throw new Error("Cannot delete user or find user");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async updateUserById(req, res) {
    if (req.body.password) {
      let password = req.body.password;
      req.body.password = await bcrypt.hash(password, 10);
    }
    try {
      let user = await UserService.updateUserById(req.userId, req.body);
      if (user) {
        res.status(200).json({
          code: "SUCCESS",
          success: "User updated successfully",
          error: null,
        });
      } else {
        throw new Error("Cannot find user or user recently updated");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async getAllUsers(req, res) {
    try {
      let users = await UserService.getAllUsers();
      if (user) {
        res.status(200).json({
          code: "SUCCESS",
          success: users,
          error: null,
        });
      } else {
        throw new Error("Cannot get all users");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }
}

module.exports = UserController;
