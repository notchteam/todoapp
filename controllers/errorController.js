module.exports = (error, req, res, next) => {
  res.status(500).json({
    code: "FAILED",
    success: null,
    error: error.message || "Cannot access the server",
  });
};
