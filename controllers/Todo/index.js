const TodoService = require("../../services/Todo");
class TodoController {
  static async createTodoByUserId(req, res) {
    try {
      let todo = await TodoService.createTodoByUserId(req.userId, req.body);
      if (todo) {
        res.status(200).json({
          code: "SUCCESS",
          success: todo,
          error: null,
        });
      } else {
        throw new Error("Cannot create a Todo at the moment");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async getTodoByUserId(req, res) {
    try {
      let todo = await TodoService.getTodoByUserId(req.params.id, req.userId);
      if (todo.length) {
        res.status(200).json({
          code: "SUCCESS",
          success: todo,
          error: null,
        });
      } else {
        throw new Error(
          "Cannot find todo or user doesn't have  this todo anymore"
        );
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async deleteTodoByUserId(req, res) {
    try {
      let deletedTodo = await TodoService.deleteTodoByUserId(
        req.params.id,
        req.userId
      );
      if (deletedTodo.length) {
        res.status(200).json({
          code: "SUCCESS",
          success: deletedTodo,
          error: null,
        });
      } else {
        throw new Error(
          "Cannot find todo or user doesn't have this todo anymore"
        );
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async updateTodoByUserId(req, res) {
    try {
      let todo = await TodoService.updateTodoByUserId(
        req.params.id,
        req.body,
        req.userId
      );
      if (todo) {
        res.status(200).json({
          code: "SUCCESS",
          success: "Todo successfully updated",
          error: null,
        });
      } else {
        throw new Error("Cannot update todo or todo recently updated");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }

  static async getAllTodosByUserId(req, res) {
    try {
      let todos = await TodoService.getAllTodosByUserId(req.userId);
      if (todos) {
        res.status(200).json({
          code: "SUCCESS",
          success: todos,
          error: null,
        });
      } else {
        throw new Error("Cannot get all todos");
      }
    } catch (error) {
      res.status(401).json({
        code: "FAILURE",
        success: null,
        error: error.message,
      });
    }
  }
}

module.exports = TodoController;
