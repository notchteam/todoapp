const User = require("./User");
const { Sequelize } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  let Todo = sequelize.define(
    "todos",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      userId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      description: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      image: {
        type: DataTypes.STRING,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      timestamps: true,
    }
  );
  // Todo.belongsTo(User(sequelize, DataTypes)); // Will add userId to Todo
  return Todo;
};
