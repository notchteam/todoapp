const express = require("express");
const routes = require("./routes");
const middlewares = require("./middlewares");
const app = express();
const port = 3000;
const RedisDB = require("./database/redis/redis");
const { MySQLDB } = require("./database/sequelize/sequelize");

// Connection to Redis DB
RedisDB.connect();

// Connection to MySQL DB
MySQLDB.connect();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
middlewares(app);
routes(app);
app.listen(process.env.PORT || port, () => {});
