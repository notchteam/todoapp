const { db } = require("../../database/sequelize/sequelize");
const Todo = db.Todo;
class TodoService {
  static async createTodoByUserId(id, todo) {
    try {
      const newTodo = await Todo.create({
        userId: id,
        description: todo.description,
        title: todo.title,
        image: todo.image,
      });
      return newTodo;
    } catch (error) {
      return null;
    }
  }

  static async getTodoByUserId(id, userId) {
    try {
      const todo = await Todo.findAll({
        where: {
          id: id,
          userId: userId,
        },
      });
      return todo;
    } catch (error) {
      return null;
    }
  }

  static async deleteTodoByUserId(id, userId) {
    try {
      const todo = await Todo.findAll({
        where: {
          id: id,
          userId: userId,
        },
      });
      Todo.destroy({
        where: {
          id: id,
          userId: userId,
        },
      });
      return todo;
    } catch (error) {
      return null;
    }
  }

  static async updateTodoByUserId(id, todo, userId) {
    let todoUpdate = {};
    for (const [key, value] of Object.entries(todo)) {
      if (value) {
        todoUpdate[key] = value;
      }
    }
    try {
      const [numberOfAffectedRows, affectedRows] = await Todo.update(
        todoUpdate,
        {
          where: { id: id, userId: userId },
          returning: true, // needed for affectedRows to be populated
          plain: true, // makes sure that the returned instances are just plain objects
        }
      );
      return affectedRows;
    } catch (error) {
      return null;
    }
  }

  static async getAllTodos() {
    try {
      return await Todo.findAll();
    } catch (error) {
      return null;
    }
  }
}

module.exports = TodoService;
