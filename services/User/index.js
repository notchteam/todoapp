const { db } = require("../../database/sequelize/sequelize");
const User = db.User;
const Todo = db.Todo;
class UserService {
  static async createUser(details) {
    try {
      const user = await User.create({
        email: details.email,
        name: details.name,
        password: details.password,
      });
      user[0].password = null;
      return user;
    } catch (error) {
      return null;
    }
  }

  static async getUserByIdWithTodos(id) {
    try {
      const user = await User.findAll({
        where: {
          id: id,
        },
        include: ["todos"],
      });
      user[0].password = null;
      return user;
    } catch (error) {
      return null;
    }
  }

  static async getUserByIdWithoutTodos(id) {
    try {
      const user = await User.findAll({
        where: {
          id: id,
        },
      });
      user[0].password = null;
      return user;
    } catch (error) {
      return null;
    }
  }

  static async deleteUserById(id) {
    try {
      const user = await User.findAll({
        where: {
          id: id,
        },
      });
      User.destroy({
        where: {
          id: id,
        },
      });
      user[0].password = null;
      return user;
    } catch (error) {
      return null;
    }
  }

  static async updateUserById(id, user) {
    let userUpdate = {};
    for (const [key, value] of Object.entries(user)) {
      if (value) {
        userUpdate[key] = value;
      }
    }
    try {
      const [numberOfAffectedRows, affectedRows] = await User.update(
        userUpdate,
        {
          where: { id: id },
          returning: true, // needed for affectedRows to be populated
          plain: true, // makes sure that the returned instances are just plain objects
        }
      );
      return affectedRows;
    } catch (error) {
      return null;
    }
  }

  static async getAllUsers() {
    try {
      return await User.findAll();
    } catch (error) {
      return null;
    }
  }
}

module.exports = UserService;
