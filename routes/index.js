const todo = require("./Todo");
module.exports = (app) => {
  app.use("/api/v1/todo", todo);
};
