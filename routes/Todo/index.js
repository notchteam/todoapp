const router = require("express").Router();
const todoSecuredRoutes = require("./todoSecuredRoutes");
const userSecuredRoutes = require("./userSecuredRoutes");
const UserController = require("../../controllers/User");
const { createUser, login, logout } = UserController;
const verifyToken = require("../../middlewares/verifyToken");

router.get("/", (req, res) => {
  res.status(200).json({
    code: "SUCCESS",
    message: "Keep Track your activities and always stay organized",
    error: null,
  });
});

router
  .post("/signup", createUser)
  .post("/login", login)
  .post("/logout", verifyToken, logout);
router.use("/", verifyToken, userSecuredRoutes);
router.use("/", verifyToken, todoSecuredRoutes);

module.exports = router;
