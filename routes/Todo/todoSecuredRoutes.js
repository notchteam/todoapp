const router = require("express").Router();
const TodoController = require("../../controllers/Todo");
const {
  getTodoByUserId,
  updateTodoByUserId,
  deleteTodoByUserId,
  createTodoByUserId,
} = TodoController;

router
  .get("/:id", getTodoByUserId)
  .put("/:id", updateTodoByUserId)
  .delete("/:id", deleteTodoByUserId)
  .post("/new", createTodoByUserId);

module.exports = router;
