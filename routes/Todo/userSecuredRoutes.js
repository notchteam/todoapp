const router = require("express").Router();
const UserController = require("../../controllers/User");
const {
  deleteUserById,
  updateUserById,
  getUserByIdWithTodos,
  getUserByIdWithoutTodos,
} = UserController;

router
  .delete("/user", deleteUserById)
  .put("/user", updateUserById)
  .get("/user/todos", getUserByIdWithTodos)
  .get("/user", getUserByIdWithoutTodos);

module.exports = router;
